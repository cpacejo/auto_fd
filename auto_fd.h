#ifndef CTP_AUTO_FD_H
#define CTP_AUTO_FD_H

#include <algorithm>
#include <chrono>
#include <cstddef>
#include <memory>
extern "C"
{
#include <semaphore.h>
#include <unistd.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <sys/types.h>
}

//
// INTERFACE
//

namespace ctp
{
  class auto_fd
  {
  public:
    class ofd
    {
    public:
      ofd() = delete;
      ofd(const ofd&) = delete;
      ofd& operator=(const ofd&) = delete;

      // Interface to flock(2), compatible with std::lock_guard and std::unique_lock.
      // However, note that using either of the above concurrently on multiple copies
      // of the same FD is almost certainly the wrong thing to do, since an unlock
      // affects the underlying (shared) open file description.
      // FIXME: technically there is a SECOND layer of indirection here, since
      // these are *file* locks.  We should probably represent this.
      void lock();
      void unlock();
      bool try_lock();
      void lock_shared();
      void unlock_shared();
      bool try_lock_shared();

      #ifdef __linux__
      // Compares the underlying open file description, using kcmp(2).
      friend int compare(const ofd&, const ofd&);
      #endif

    private:
      explicit ofd(int) noexcept;
      operator int() const noexcept;
      ofd& operator=(int) noexcept;

      int m_fd;

      friend auto_fd;
    };

    auto_fd() noexcept = default;

    // Take ownership of a raw FD.  The FD is NOT dup(2)ed and should
    // NOT be close(2)d by the application.  It is legal to pass an "invalid"
    // (negative) FD; this results in a "dummy" auto_fd.
    explicit auto_fd(int fd) noexcept;

    // Copy (via dup(2)) another auto_fd.  The copy has its FD_CLOEXEC flag set,
    // regardless of whether the original did.
    auto_fd(const auto_fd &);

    // Move from another auto_fd.  No system calls involved.
    auto_fd(auto_fd &&) noexcept;

    // close(2)s the existing FD (if any).
    ~auto_fd();

    // Copy (via dup(2) or dup3(2)) another auto_fd, close(2)ing the existing
    // FD (if any).  The copy has its FD_CLOEXEC flag set, regardless of whether
    // the original did.
    auto_fd &operator=(const auto_fd &);

    // Move from another auto_fd, close(2)ing the existing FD (if any).
    auto_fd &operator=(auto_fd &&other) noexcept;

    // Checks whether the FD is "valid" (nonnegative).  (The auto_fd is always
    // in a valid state, even if the underlying FD is not.)
    bool valid() const noexcept;

    // Deleted to prevent confusion.  Use .valid() instead.
    operator bool() const noexcept = delete;

    // Returns (but does not release) the underlying FD.
    operator int() const noexcept;

    // Returns, and releases ownership of, the underlying FD, resetting the
    // auto_fd in the process.
    int release() noexcept;

    // close(2) the existing FD (if any) and take ownership of another (or
    // none at all).  `x.reset(fd)` is identical to `x = autofd(fd)`.
    void reset(int fd = -1) noexcept;

    // `x.reset_dup(fd)` is identical to `x = autofd::dup(fd)`, but uses dup3(2)
    // if possible.
    void reset_dup(int fd);

    // Explicitly close(2) the FD, if it is valid.  Will throw
    // std::system_error if the underlying system call fails, in which case
    // auto_fd is left in an undefined but legal state.  (This is the only
    // method which does NOT ignore close(2) errors.)  This object is
    // reset after a successful close.
    void close();

    ofd& operator*() const noexcept { return const_cast<ofd&>(m_fd); }
    ofd* operator->() const noexcept { return const_cast<ofd*>(&m_fd); }

  private:
    ofd m_fd{-1};
  };

  // Create a new auto_fd as a dup(2) of an existing FD.  (So,
  // unlike the constructor, does NOT take ownership.)  The copy has its
  // FD_CLOEXEC flag set, regardless of whether the original did.
  auto_fd dup(int fd);

  #ifdef __linux__
  int compare(const auto_fd::ofd &, const auto_fd::ofd &);
  bool operator==(const auto_fd::ofd &, const auto_fd::ofd &);
  bool operator!=(const auto_fd::ofd &, const auto_fd::ofd &);
  bool operator<(const auto_fd::ofd &, const auto_fd::ofd &);
  bool operator<=(const auto_fd::ofd &, const auto_fd::ofd &);
  bool operator>(const auto_fd::ofd &, const auto_fd::ofd &);
  bool operator>=(const auto_fd::ofd &, const auto_fd::ofd &);
  #endif


  class auto_mmap
  {
  public:
    // flags should not include MAP_FIXED, MAP_PRIVATE, or MAP_ANONYMOUS.

    auto_mmap() noexcept;
    auto_mmap(void *addr, std::size_t length, int prot, int flags, auto_fd fd, ::off_t offset = 0) noexcept;
    auto_mmap(const auto_mmap &);
    auto_mmap(auto_mmap &&) noexcept;

    static auto_mmap map(std::size_t length, int prot, int flags, auto_fd fd, ::off_t offset = 0);

    ~auto_mmap();

    auto_mmap &operator=(const auto_mmap &);
    auto_mmap &operator=(auto_mmap &&) noexcept;

    bool valid() const noexcept;
    void *get() const noexcept;

    #ifdef __linux__
    void remap(std::size_t new_size, int flags = 0);
    #endif

    void reset() noexcept;
    void reset(void *addr, std::size_t length, int prot, int flags, const auto_fd &fd, ::off_t offset = 0);
    void reset(void *addr, std::size_t length, int prot, int flags, auto_fd &&fd, ::off_t offset = 0) noexcept;

    void reset_map(std::size_t length, int prot, int flags, const auto_fd &fd, ::off_t offset = 0);
    void reset_map(std::size_t length, int prot, int flags, auto_fd &&fd, ::off_t offset = 0);

    void unmap() noexcept;

  private:
    std::size_t m_length;
    int m_prot, m_flags;
    auto_fd m_fd;
    ::off_t m_offset;
    void *m_addr;
  };


  struct fork_t { }; constexpr fork_t fork = fork_t();

  class process
  {
  public:
    using id = ::pid_t;
    using native_handle_type = ::pid_t;

    process() noexcept = default;
    process(fork_t);

    process(const process&) = delete;
    process(process&&) noexcept;

    ~process();

    process& operator=(const process&) = delete;
    process& operator=(process&&) noexcept;

    id get_id() const noexcept;

    bool waitable() const noexcept;
    int wait();

    void reset();

    native_handle_type native_handle() const noexcept;
    native_handle_type release_native_handle() noexcept;
    void reset_native_handle(native_handle_type);

  private:
    ::pid_t m_pid = 0;
  };

  namespace this_process
  {
    process::id get_id() noexcept;
  }


  class unnamed_semaphore
  {
  public:
    unnamed_semaphore(bool pshared = false, unsigned value = 0);
    ~unnamed_semaphore();

    unnamed_semaphore() = delete;
    unnamed_semaphore(const unnamed_semaphore&) = delete;
    unnamed_semaphore& operator=(const unnamed_semaphore&) = delete;

    void post();
    void wait();
    bool try_wait();
    bool try_wait_until(const std::chrono::system_clock::time_point&);
    template<typename Rep, typename Period>
    bool try_wait_for(const std::chrono::duration<Rep, Period>&);
    int get_value() const noexcept;

  protected:
    mutable ::sem_t m_sem;
  };

  class const_named_semaphore_ref
  {
  public:
    const_named_semaphore_ref(const char* name, int oflag = 0, ::mode_t mode = 0777, unsigned value = 0);
    const_named_semaphore_ref(const const_named_semaphore_ref&) noexcept = default;

    const_named_semaphore_ref& operator=(const const_named_semaphore_ref&) = delete;

    int get_value() const noexcept;

  protected:
    const_named_semaphore_ref(const_named_semaphore_ref&&) noexcept = default;
    const_named_semaphore_ref& operator=(const_named_semaphore_ref&&) noexcept = default;

    std::shared_ptr<::sem_t> m_sem;
  };

  class named_semaphore;

  class named_semaphore_ref: public const_named_semaphore_ref
  {
  public:
    named_semaphore_ref(const char* name, int oflag = 0, ::mode_t mode = 0777, unsigned value = 0);
    named_semaphore_ref(const named_semaphore_ref&) noexcept = default;

    named_semaphore_ref& operator=(const named_semaphore_ref&) = delete;

    void post() const;
    void wait() const;
    bool try_wait() const;
    bool try_wait_until(const std::chrono::system_clock::time_point&) const;
    template<typename Rep, typename Period>
    bool try_wait_for(const std::chrono::duration<Rep, Period>&) const;

  protected:
    named_semaphore_ref(named_semaphore_ref&&) noexcept = default;
    named_semaphore_ref& operator=(named_semaphore_ref&&) noexcept = default;

    friend named_semaphore;
  };

  class named_semaphore
  {
  public:
    named_semaphore(const char* name, int oflag = O_CREAT | O_EXCL, ::mode_t mode = 0777, unsigned value = 0);
    named_semaphore(named_semaphore&&) noexcept;
    ~named_semaphore();

    named_semaphore& operator=(named_semaphore&&) noexcept;

    void post();
    void wait();
    bool try_wait();
    bool try_wait_until(const std::chrono::system_clock::time_point&);
    template<typename Rep, typename Period>
    bool try_wait_for(const std::chrono::duration<Rep, Period>&);
    int get_value() const noexcept;

    operator named_semaphore_ref() noexcept;
    operator const_named_semaphore_ref() const noexcept;

    named_semaphore_ref release() noexcept;

    // Manually clean up the named semaphore, after e.g. a process crash.
    static void unlink(const char* name);

  protected:
    // NOTE!  Order is important; we rely on no exceptions tripping after
    // semaphore is created.
    std::string m_name;
    named_semaphore_ref m_sem;
  };

  // Silly adaptor class which adapts a semaphore as a mutex in the obvious way.
  // Note that you MUST explicitly initialize the semaphore with a value of 1!
  template<typename S = unnamed_semaphore>
  class semaphore_mutex
  {
  public:
    using semaphore_type = S;

    template<typename... Args>
    explicit semaphore_mutex(Args&&... args): m_sem(std::forward(args)...) { }

    void lock() { m_sem.wait(); }
    void unlock() { m_sem.post(); }
    bool try_lock() { return m_sem.try_wait(); }
    template<typename Clock, typename Dur>
    bool try_lock_until(const std::chrono::time_point<Clock, Dur>& tp)
    { return m_sem.try_wait_until(tp); }
    template<typename Rep, typename Period>
    bool try_lock_for(const std::chrono::duration<Rep, Period>& dur)
    { return m_sem.try_wait_for(dur); }

  protected:
    S m_sem;
  };
}


//
// IMPLEMENTATION
//

#include <exception>
#include <utility>

namespace ctp
{
  inline auto_fd::ofd::ofd(const int fd) noexcept: m_fd(fd) { }
  inline auto_fd::ofd::operator int() const noexcept { return m_fd; }
  inline auto_fd::ofd& auto_fd::ofd::operator=(const int fd) noexcept { m_fd = fd; return *this; }

  inline auto_fd::auto_fd(const int fd) noexcept: m_fd(fd) { }
  inline auto_fd::auto_fd(const auto_fd &other): auto_fd() { reset_dup(other.m_fd); }
  inline auto_fd::auto_fd(auto_fd &&other) noexcept: auto_fd(other.m_fd) { other.m_fd = -1; }
  inline auto_fd dup(const int fd) { auto_fd ret; ret.reset_dup(fd); return ret; }
  inline auto_fd::~auto_fd() { if (m_fd >= 0) ::close(m_fd); }
  inline auto_fd &auto_fd::operator=(const auto_fd &other) { reset_dup(other.m_fd); return *this; }
  inline auto_fd &auto_fd::operator=(auto_fd &&other) noexcept { reset(other.release()); return *this; }
  inline bool auto_fd::valid() const noexcept { return m_fd >= 0; }
  inline auto_fd::operator int() const noexcept { return m_fd; }
  inline int auto_fd::release() noexcept { const int fd = m_fd; m_fd = -1; return fd; }
  inline void auto_fd::reset(const int fd) noexcept { this->~auto_fd(); new(this) auto_fd(fd); }
  #ifdef __linux__
  inline bool operator==(const auto_fd::ofd &lhs, const auto_fd::ofd &rhs) { return compare(lhs, rhs) == 0; }
  inline bool operator!=(const auto_fd::ofd &lhs, const auto_fd::ofd &rhs) { return compare(lhs, rhs) != 0; }
  inline bool operator<(const auto_fd::ofd &lhs, const auto_fd::ofd &rhs) { return compare(lhs, rhs) < 0; }
  inline bool operator<=(const auto_fd::ofd &lhs, const auto_fd::ofd &rhs) { return compare(lhs, rhs) <= 0; }
  inline bool operator>(const auto_fd::ofd &lhs, const auto_fd::ofd &rhs) { return compare(lhs, rhs) > 0; }
  inline bool operator>=(const auto_fd::ofd &lhs, const auto_fd::ofd &rhs) { return compare(lhs, rhs) >= 0; }
  #endif

  inline auto_mmap::auto_mmap() noexcept: m_addr(MAP_FAILED) { }
  inline auto_mmap::auto_mmap(void *const addr, const std::size_t length, const int prot, const int flags,
      auto_fd fd, const ::off_t offset) noexcept
    : m_length(length), m_prot(prot), m_flags(flags), m_fd(std::move(fd)), m_offset(offset),
      m_addr(addr) { }
  inline auto_mmap::auto_mmap(const auto_mmap &other): auto_mmap()
  { reset_map(other.m_length, other.m_prot, other.m_flags, other.m_fd, other.m_offset); }
  inline auto_mmap::auto_mmap(auto_mmap &&other) noexcept
    : auto_mmap(other.m_addr, other.m_length, other.m_prot, other.m_flags, std::move(other.m_fd), other.m_offset)
  { other.m_addr = MAP_FAILED; }
  inline auto_mmap auto_mmap::map(const std::size_t length, const int prot, const int flags,
      auto_fd fd, const ::off_t offset)
  { auto_mmap ret; ret.reset_map(length, prot, flags, std::move(fd), offset); return ret; }
  inline auto_mmap::~auto_mmap() { if (m_addr != MAP_FAILED) ::munmap(m_addr, m_length); }
  inline auto_mmap &auto_mmap::operator=(const auto_mmap &other)
  { reset_map(other.m_length, other.m_prot, other.m_flags, other.m_fd, other.m_offset); return *this; }
  inline auto_mmap &auto_mmap::operator=(auto_mmap &&other) noexcept
  {
    reset(other.m_addr, other.m_length, other.m_prot, other.m_flags, std::move(other.m_fd), other.m_offset);
    other.m_addr = MAP_FAILED;
    return *this;
  }
  inline bool auto_mmap::valid() const noexcept { return m_addr != MAP_FAILED; }
  inline void *auto_mmap::get() const noexcept { return m_addr; }
  inline void auto_mmap::reset() noexcept { reset(nullptr, 0, 0, 0, auto_fd()); }
  inline void auto_mmap::unmap() noexcept { reset(); }

  inline process::process(process&& other) noexcept: m_pid(other.m_pid) { other.m_pid = 0; }
  inline process::~process() { if (waitable()) std::terminate(); }
  inline process& process::operator=(process&& other) noexcept
  { if (waitable()) std::terminate(); m_pid = other.release_native_handle(); return *this; }
  inline process::id process::get_id() const noexcept { return m_pid; }
  inline bool process::waitable() const noexcept { return m_pid > 0; }
  inline void process::reset() { reset_native_handle(0); }
  inline process::native_handle_type process::native_handle() const noexcept { return m_pid; }
  inline process::native_handle_type process::release_native_handle() noexcept
  { const auto pid = m_pid; m_pid = 0; return pid; }
  inline void process::reset_native_handle(const native_handle_type pid)
  { if (waitable()) std::terminate(); m_pid = pid; }

  template<typename Rep, typename Period>
  inline bool unnamed_semaphore::try_wait_for(const std::chrono::duration<Rep, Period>& dur)
  { return try_wait_until(std::chrono::system_clock::now() + dur); }

  template<typename Rep, typename Period>
  inline bool named_semaphore_ref::try_wait_for(const std::chrono::duration<Rep, Period>& dur) const
  { return try_wait_until(std::chrono::system_clock::now() + dur); }

  inline named_semaphore::named_semaphore(
      const char* const name, const int oflag, const ::mode_t mode, const unsigned value)
    : m_name(name), m_sem(name, oflag, mode, value) { }
  inline named_semaphore::named_semaphore(named_semaphore&& other) noexcept
    : m_name(std::move(other.m_name)), m_sem(std::move(other.m_sem))
  { other.m_name.clear(); }
  inline named_semaphore& named_semaphore::operator=(named_semaphore&& other) noexcept
  {
    if (this != &other) { this->~named_semaphore(); new(this) named_semaphore(std::move(other)); }
    return *this;
  }
  inline void named_semaphore::post() { m_sem.post(); }
  inline void named_semaphore::wait() { m_sem.wait(); }
  inline bool named_semaphore::try_wait() { return m_sem.try_wait(); }
  inline bool named_semaphore::try_wait_until(const std::chrono::system_clock::time_point& tp)
  { return m_sem.try_wait_until(tp); }
  template<typename Rep, typename Period>
  inline bool named_semaphore::try_wait_for(const std::chrono::duration<Rep, Period>& dur)
  { return m_sem.try_wait_for(dur); }
  inline int named_semaphore::get_value() const noexcept { return m_sem.get_value(); }
  inline named_semaphore::operator named_semaphore_ref() noexcept { return m_sem; }
  inline named_semaphore::operator const_named_semaphore_ref() const noexcept { return m_sem; }
  inline named_semaphore_ref named_semaphore::release() noexcept
  { m_name.clear(); return std::move(m_sem); }
}

#endif
