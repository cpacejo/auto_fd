CPPFLAGS = -std=c++11 -Wall -Wextra -Werror -Os

OBJS = auto_fd.o

auto_fd.a: $(OBJS)
	ar rcs $@ $(OBJS)

auto_fd.o: auto_fd.h
