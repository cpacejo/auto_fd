#include <cerrno>
#include <chrono>
#include <exception>
#include <system_error>
#include <utility>
extern "C"
{
#include <fcntl.h>
#include <semaphore.h>
#include <unistd.h>
#ifdef __linux__
#include <linux/kcmp.h>
#include <sys/syscall.h>
#endif
#include <sys/file.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
}
#include "auto_fd.h"

void ctp::auto_fd::ofd::lock()
{
  if (::flock(m_fd, LOCK_EX) < 0)
    throw std::system_error(errno, std::generic_category());
}

void ctp::auto_fd::ofd::unlock()
{
  if (::flock(m_fd, LOCK_UN) < 0)
    throw std::system_error(errno, std::generic_category());
}

bool ctp::auto_fd::ofd::try_lock()
{
  if (::flock(m_fd, LOCK_EX | LOCK_NB) < 0)
  {
    if (errno == EWOULDBLOCK) return false;
    throw std::system_error(errno, std::generic_category());
  }
  return true;
}

void ctp::auto_fd::ofd::lock_shared()
{
  if (::flock(m_fd, LOCK_SH) < 0)
    throw std::system_error(errno, std::generic_category());
}

void ctp::auto_fd::ofd::unlock_shared()
{
  if (::flock(m_fd, LOCK_UN) < 0)
    throw std::system_error(errno, std::generic_category());
}

bool ctp::auto_fd::ofd::try_lock_shared()
{
  if (::flock(m_fd, LOCK_SH | LOCK_NB) < 0)
  {
    if (errno == EWOULDBLOCK) return false;
    throw std::system_error(errno, std::generic_category());
  }
  return true;
}


void ctp::auto_fd::reset_dup(const int fd)
{
  if (fd < 0)
  {
    this->~auto_fd();
    new(this) auto_fd();
  }
  else if (m_fd < 0)
  {
    const int new_fd = ::fcntl(fd, F_DUPFD_CLOEXEC, 0);
    if (new_fd < 0)
      throw std::system_error(errno, std::generic_category());
    m_fd = new_fd;
  }
  else if (m_fd != fd)
  {
    #ifdef __linux__
    if (::dup3(fd, m_fd, O_CLOEXEC) < 0)
    {
      // Unlike close(), we must assume here that m_fd is still OPEN.
      // This is because dup3 swallows any errors that close() generates,
      // and guarantees that the fd is never invalid (that is, the whole
      // operation is atomic).  This means that an error basically means
      // "the dup WOULD have failed, so I didn't even close the target fd".
      throw std::system_error(errno, std::generic_category());
    }
    #else
    const int new_fd = ::fcntl(fd, F_DUPFD_CLOEXEC, 0);
    if (new_fd < 0)
      throw std::system_error(errno, std::generic_category());
    ::close(m_fd);
    m_fd = new_fd;
    #endif
  }
}

void ctp::auto_fd::close()
{
  if (m_fd < 0) return;

  if (::close(m_fd) < 0)
  {
    // According to POSIX, we should assume m_fd actually remains OPEN in
    // the case of EINTR.  But according to Linux, we should assume it is
    // closed.  Better to suffer a leak than a double-free, so we follow the
    // Linux model.
    m_fd = -1;
    throw std::system_error(errno, std::generic_category());
  }

  m_fd = -1;
}

#ifdef __linux__
int ctp::compare(const auto_fd::ofd &lhs, const auto_fd::ofd &rhs)
{
  if (lhs < 0 || rhs < 0)
    throw std::system_error(EBADF, std::generic_category());

  const pid_t pid = ::getpid();
  const int ret = ::syscall(SYS_kcmp, pid, pid, KCMP_FILE, int(lhs), int(rhs));
  if (ret < 0)
    throw std::system_error(errno, std::generic_category());

  switch (ret)
  {
  case 0: return 0;
  case 1: return -1;
  case 2: return 1;
  default: throw std::logic_error("kcmp");
  }
}
#endif


#ifdef __linux__
void ctp::auto_mmap::remap(const std::size_t new_size, const int flags)
{
  void *const addr = ::mremap(m_addr, m_length, new_size, flags);
  if (addr == MAP_FAILED) 
    throw std::system_error(errno, std::generic_category());
  m_length = new_size;
  m_addr = addr;
}
#endif

void ctp::auto_mmap::reset(void *const addr, const std::size_t length, const int prot, const int flags,
  const auto_fd &fd, const ::off_t offset)
{
  m_fd = fd;  // must come first; this can throw
  m_length = length;
  m_prot = prot;
  m_flags = flags;
  m_offset = offset;
  if (m_addr != MAP_FAILED) ::munmap(m_addr, m_length);
  m_addr = addr;
}

void ctp::auto_mmap::reset(void *const addr, const std::size_t length, const int prot, const int flags,
  auto_fd &&fd, const ::off_t offset) noexcept
{
  m_length = length;
  m_prot = prot;
  m_flags = flags;
  m_fd = std::move(fd);
  m_offset = offset;
  if (m_addr != MAP_FAILED) ::munmap(m_addr, m_length);
  m_addr = addr;
}

void ctp::auto_mmap::reset_map(const std::size_t length, const int prot, const int flags,
  const auto_fd &fd, const ::off_t offset)
{
  void *const addr = ::mmap(nullptr, length, prot, flags, fd, offset);
  if (addr == MAP_FAILED) 
    throw std::system_error(errno, std::generic_category());
  try { reset(addr, length, prot, flags, fd, offset); }
  catch (...) { ::munmap(addr, length); throw; }
}

void ctp::auto_mmap::reset_map(const std::size_t length, const int prot, const int flags,
  auto_fd &&fd, const ::off_t offset)
{
  void *const addr = ::mmap(nullptr, length, prot, flags, fd, offset);
  if (addr == MAP_FAILED) 
    throw std::system_error(errno, std::generic_category());
  reset(addr, length, prot, flags, std::move(fd), offset);
}


ctp::process::process(fork_t)
  : m_pid(::fork())
{
  if (m_pid < 0)
    throw std::system_error(errno, std::generic_category());
}

int ctp::process::wait()
{
  if (m_pid <= 0)
    throw std::system_error(EINVAL, std::generic_category());

  int ret;
  if (::waitpid(m_pid, &ret, 0) < 0)
    throw std::system_error(errno, std::generic_category());

  m_pid = 0;
  return ret;
}

ctp::process::id ctp::this_process::get_id() noexcept
{
  return ::getpid();
}


ctp::unnamed_semaphore::unnamed_semaphore(const bool pshared, const unsigned value)
{
  if (::sem_init(&m_sem, pshared, value) < 0)
    throw std::system_error(errno, std::generic_category());
}

ctp::unnamed_semaphore::~unnamed_semaphore()
{
  if (::sem_destroy(&m_sem) < 0)
    std::terminate();
}

void ctp::unnamed_semaphore::post()
{
  if (::sem_post(&m_sem) < 0)
    throw std::system_error(errno, std::generic_category());
}

void ctp::unnamed_semaphore::wait()
{
  if (::sem_wait(&m_sem) < 0)
    throw std::system_error(errno, std::generic_category());
}

bool ctp::unnamed_semaphore::try_wait()
{
  if (::sem_trywait(&m_sem) < 0)
  {
    if (errno == EAGAIN) return false;
    throw std::system_error(errno, std::generic_category());
  }
  return true;
}

bool ctp::unnamed_semaphore::try_wait_until(const std::chrono::system_clock::time_point& tp)
{
  ::timespec ts;
  ts.tv_sec = std::chrono::system_clock::to_time_t(tp);
  ts.tv_nsec = std::chrono::duration_cast<std::chrono::nanoseconds>(
      tp - std::chrono::system_clock::from_time_t(ts.tv_sec)).count();
  if (::sem_timedwait(&m_sem, &ts) < 0)
  {
    if (errno == EAGAIN) return false;
    throw std::system_error(errno, std::generic_category());
  }
  return true;
}

int ctp::unnamed_semaphore::get_value() const noexcept
{
  int ret;
  if (::sem_getvalue(&m_sem, &ret) < 0)
    std::terminate();
  return ret;
}


ctp::const_named_semaphore_ref::const_named_semaphore_ref(
    const char* const name, const int oflag, const ::mode_t mode, const unsigned value)
{
  ::sem_t *const sem = ::sem_open(name, oflag, mode, value);
  if (sem == SEM_FAILED)
    throw std::system_error(errno, std::generic_category());

  m_sem.reset(sem, ::sem_close);
}

int ctp::const_named_semaphore_ref::get_value() const noexcept
{
  int ret;
  if (::sem_getvalue(m_sem.get(), &ret) < 0)
    std::terminate();
  return ret;
}


ctp::named_semaphore_ref::named_semaphore_ref(
    const char* const name, const int oflag, const ::mode_t mode, const unsigned value)
  : const_named_semaphore_ref(name, oflag, mode, value)
{ }

void ctp::named_semaphore_ref::post() const
{
  if (::sem_post(m_sem.get()) < 0)
    throw std::system_error(errno, std::generic_category());
}

void ctp::named_semaphore_ref::wait() const
{
  if (::sem_wait(m_sem.get()) < 0)
    throw std::system_error(errno, std::generic_category());
}

bool ctp::named_semaphore_ref::try_wait() const
{
  if (::sem_trywait(m_sem.get()) < 0)
  {
    if (errno == EAGAIN) return false;
    throw std::system_error(errno, std::generic_category());
  }
  return true;
}

bool ctp::named_semaphore_ref::try_wait_until(const std::chrono::system_clock::time_point& tp) const
{
  ::timespec ts;
  ts.tv_sec = std::chrono::system_clock::to_time_t(tp);
  ts.tv_nsec = std::chrono::duration_cast<std::chrono::nanoseconds>(
      tp - std::chrono::system_clock::from_time_t(ts.tv_sec)).count();
  if (::sem_timedwait(m_sem.get(), &ts) < 0)
  {
    if (errno == EAGAIN) return false;
    throw std::system_error(errno, std::generic_category());
  }
  return true;
}


ctp::named_semaphore::~named_semaphore()
{
  if (!m_name.empty() && ::shm_unlink(m_name.c_str()) < 0)
    std::terminate();
}

void ctp::named_semaphore::unlink(const char* const name)
{
  if (::sem_unlink(name) < 0)
    throw std::system_error(errno, std::generic_category());
}
